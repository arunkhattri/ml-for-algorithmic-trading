#+title: NSE Banks: Pvt. Banks or PSU Banks
#+setupfile: ~/Dropbox/orgFiles/emacs-stuff/ak-latex-standard.org

An analysis to find One should go for private banks or PSU bank shares...

* Pre-requisite
loading required packages for the analysis

#+begin_src python :session *py-session :results output :exports both :cache yes
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import mplfinance as mpf
import yfinance as yf
import talib as ta
#+end_src

#+RESULTS[b56b416d090160ba07c7c90b1ae7060eb5b52ac8]:

* Data

** Private Bank Index

Getting *Nifty Private Bank index* for last 5 years...

#+begin_src python :session *py-session :results output :exports both :cache yes
nifty_pvt_banks = yf.Ticker("NIFTY_PVT_BANK.NS")
pvt_bank_hist = nifty_pvt_banks.history(period="5y", interval="1d", actions=False)
pvt_bank_hist.info()
#+end_src

#+RESULTS[af11e80194853ecd51cb84b502a85abc29bf4092]:
#+begin_example
/home/arunkhattri/gitlab/arunkhattri/ml-for-algorithmic-trading/.venv/lib/python3.11/site-packages/yfinance/utils.py:775: FutureWarning: The 'unit' keyword in TimedeltaIndex construction is deprecated and will be removed in a future version. Use pd.to_timedelta instead.
  df.index += _pd.TimedeltaIndex(dst_error_hours, 'h')
<class 'pandas.core.frame.DataFrame'>
DatetimeIndex: 1219 entries, 2019-02-12 00:00:00+05:30 to 2024-02-12 00:00:00+05:30
Data columns (total 5 columns):
 #   Column  Non-Null Count  Dtype
---  ------  --------------  -----
 0   Open    1219 non-null   float64
 1   High    1219 non-null   float64
 2   Low     1219 non-null   float64
 3   Close   1219 non-null   float64
 4   Volume  1219 non-null   int64
dtypes: float64(4), int64(1)
memory usage: 57.1 KB
#+end_example

have a first hand look on the price movement of Nifty Private Banks over the period of 5 years...

#+begin_src python :session *py-session :results file graphics :file ../../img/pvt_banks.png :exports both :cache yes
# fig = mpf.figure(style='yahoo', figsize=(8, 9))
# ax = fig.subplots()
# mpf.plot(pvt_bank_hist, type='line', ax=ax)
fig, ax = plt.subplots(figsize=(8, 6))
fig.suptitle("Pvt. Bank Index")
ax.plot(pvt_bank_hist['Close'])
ax.grid(axis='y')
fig
#+end_src

#+RESULTS[bf9c9c3d7f5406b5f933c89e7782b0c5c9c59bdc]:
[[file:../../img/pvt_banks.png]]

*Relative Strength Index (RSI)* of private banks

#+begin_src python :session *py-session :results graphics file :file ../../img/rsi_pvt_bank.png :exports both :cache yes
pvt_bank_hist['RSI']  = ta.RSI(pvt_bank_hist['Close'])
fig, axs = plt.subplots(2, 1, gridspec_kw={"height_ratios": [3, 1]}, figsize=(10,6))
fig.suptitle("Pvt Bank Index and RSI")
axs[0].plot(pvt_bank_hist['Close'])
axs[0].grid(axis='y')
axs[1].axhline(y=70, color='r', linestyle="--")
axs[1].axhline(y=30, color='g', linestyle="--")
axs[1].plot(pvt_bank_hist['RSI'], color='orange')
fig
#+end_src

#+RESULTS[368c40a58ceb497bc0771ed2e3ae0ce0290aa6b6]:
[[file:../../img/rsi_pvt_bank.png]]


*Average Directional Movemnet Index (ADX)* for Pvt. Banks.

#+begin_src python :session *py-session :results graphics file :file ../../img/rsi_adx_pvt_bank.png :exports both :cache yes
pvt_bank_hist['ADX']  = ta.ADX(pvt_bank_hist['High'], pvt_bank_hist['Low'],
                               pvt_bank_hist['Close'])
fig, axs = plt.subplots(3, 1, gridspec_kw={"height_ratios": [3, 1, 1]}, figsize=(10,8))
fig.suptitle("Pvt Bank Index, RSI & ADX")
axs[0].plot(pvt_bank_hist['Close'])
axs[0].grid(axis='y')
axs[1].axhline(y=70, color='r', linestyle="--")
axs[1].axhline(y=30, color='g', linestyle="--")
axs[1].plot(pvt_bank_hist['RSI'], color='blue')
axs[1].set_ylabel("RSI")
axs[2].axhline(y=70, color='r', linestyle="--")
axs[2].axhline(y=30, color='g', linestyle="--")
axs[2].plot(pvt_bank_hist['ADX'], color='blue')
axs[2].set_ylabel("ADX")
fig
#+end_src

#+RESULTS[bfe221e07d8ae91c9c83f9eb3d77c53bf0024ef9]:
[[file:../../img/rsi_adx_pvt_bank.png]]

** PSU Bank Index
Now, get the data from PSU Bank's ...

#+begin_src python :session *py-session :results output :exports both
nifty_psu_banks = yf.Ticker("^CNXPSUBANK")
psu_bank_hist = nifty_psu_banks.history(period="5y", interval="1d", actions=False)
psu_bank_hist.info()
#+end_src

#+RESULTS:
#+begin_example
/home/arunkhattri/gitlab/arunkhattri/ml-for-algorithmic-trading/.venv/lib/python3.11/site-packages/yfinance/utils.py:775: FutureWarning: The 'unit' keyword in TimedeltaIndex construction is deprecated and will be removed in a future version. Use pd.to_timedelta instead.
  df.index += _pd.TimedeltaIndex(dst_error_hours, 'h')
<class 'pandas.core.frame.DataFrame'>
DatetimeIndex: 1220 entries, 2019-02-12 00:00:00+05:30 to 2024-02-12 00:00:00+05:30
Data columns (total 5 columns):
 #   Column  Non-Null Count  Dtype
---  ------  --------------  -----
 0   Open    1220 non-null   float64
 1   High    1220 non-null   float64
 2   Low     1220 non-null   float64
 3   Close   1220 non-null   float64
 4   Volume  1220 non-null   int64
dtypes: float64(4), int64(1)
memory usage: 57.2 KB
#+end_example

visualizing the price movement of psu bank's ...


#+begin_src python :session *py-session :results file graphics :file ../../img/psu_banks.png :exports both :cache yes
fig, ax = plt.subplots(figsize = (8, 6))
fig.suptitle("PSU Bank Index")
ax.plot(psu_bank_hist['Close'])
ax.grid(axis='y')
fig
#+end_src

#+RESULTS[ec3aa8b62fe5bf41aeb349d096d0729450638ec5]:
[[file:../../img/psu_banks.png]]

*Relative Strength Index (RSI)* of PSU banks

#+begin_src python :session *py-session :results graphics file :file ../../img/rsi_psu_bank.png :exports both :cache yes
psu_bank_hist['RSI']  = ta.RSI(psu_bank_hist['Close'])
fig, axs = plt.subplots(2, 1, gridspec_kw={"height_ratios": [3, 1]}, figsize=(10,6))
fig.suptitle("PSU Bank Index and RSI")
axs[0].plot(psu_bank_hist['Close'])
axs[0].grid(axis='y')
axs[1].axhline(y=70, color='r', linestyle="--")
axs[1].axhline(y=30, color='g', linestyle="--")
axs[1].plot(psu_bank_hist['RSI'], color='orange')
fig
#+end_src

#+RESULTS[3b1a41131493198d3da09859e073a137b7f9847d]:
[[file:../../img/rsi_psu_bank.png]]


*Average Directional Movemnet Index (ADX)* for Pvt. Banks.

#+begin_src python :session *py-session :results graphics file :file ../../img/adx_psu_bank.png :exports both :cache yes
psu_bank_hist['ADX']  = ta.ADX(psu_bank_hist['High'], psu_bank_hist['Low'],
                               psu_bank_hist['Close'])
fig, axs = plt.subplots(3, 1, gridspec_kw={"height_ratios": [3, 1, 1]}, figsize=(10,8))
fig.suptitle("PSU Bank Index, RSI and ADX")
axs[0].plot(psu_bank_hist['Close'])
axs[0].grid(axis='y')
axs[1].axhline(y=70, color='r', linestyle="--")
axs[1].axhline(y=30, color='g', linestyle="--")
axs[1].plot(psu_bank_hist['RSI'], color='blue')
axs[1].set_ylabel("RSI")
axs[2].axhline(y=70, color='r', linestyle="--")
axs[2].axhline(y=30, color='g', linestyle="--")
axs[2].plot(psu_bank_hist['ADX'], color='blue')
axs[2].set_ylabel("ADX")
fig
#+end_src

#+RESULTS[f0752ab921890a3622ca71b0e04cc609e08bddb0]:
[[file:../../img/adx_psu_bank.png]]

** Nifty Bank Index

and finally for nifty bank index ...

#+begin_src python :session *py-session :results output :exports both :cache yes
nifty_banks = yf.Ticker("^NSEBANK")
bank_hist = nifty_banks.history(period="5y", interval="1d", actions=False)
bank_hist.info()
#+end_src

#+RESULTS[99bad9760e806db23ee26cbe3a708e2d5c73755f]:
#+begin_example
/home/arunkhattri/gitlab/arunkhattri/ml-for-algorithmic-trading/.venv/lib/python3.11/site-packages/yfinance/utils.py:775: FutureWarning: The 'unit' keyword in TimedeltaIndex construction is deprecated and will be removed in a future version. Use pd.to_timedelta instead.
  df.index += _pd.TimedeltaIndex(dst_error_hours, 'h')
<class 'pandas.core.frame.DataFrame'>
DatetimeIndex: 1228 entries, 2019-02-12 00:00:00+05:30 to 2024-02-12 00:00:00+05:30
Data columns (total 5 columns):
 #   Column  Non-Null Count  Dtype
---  ------  --------------  -----
 0   Open    1228 non-null   float64
 1   High    1228 non-null   float64
 2   Low     1228 non-null   float64
 3   Close   1228 non-null   float64
 4   Volume  1228 non-null   int64
dtypes: float64(4), int64(1)
memory usage: 57.6 KB
#+end_example

Visualizing nifty banks..

#+begin_src python :session *py-session :results file graphics :file ../../img/psu_banks.png :exports both :cache yes
fig, ax = plt.subplots()
fig.suptitle("NIFTY Bank Index")
ax.plot(bank_hist['Close'])
ax.grid(axis='y')
fig
#+end_src

#+RESULTS[2a844671c7701a52523c762888ce80ce9c24200d]:
[[file:../../img/psu_banks.png]]

*Relative Strength Index (RSI)* of NIFTY bank index

#+begin_src python :session *py-session :results graphics file :file ../../img/rsi_nifty_bank.png :exports both :cache yes
bank_hist['RSI']  = ta.RSI(bank_hist['Close'])
fig, axs = plt.subplots(2, 1, gridspec_kw={"height_ratios": [3, 1]}, figsize=(10,6))
fig.suptitle("NIFTY Bank Index and RSI")
axs[0].plot(bank_hist['Close'])
axs[0].grid(axis='y')
axs[1].axhline(y=70, color='r', linestyle="--")
axs[1].axhline(y=30, color='g', linestyle="--")
axs[1].plot(bank_hist['RSI'], color='orange')
fig
#+end_src

#+RESULTS[18413cb9808d828c143e445ab3a1ec3bb1ae1e8a]:
[[file:../../img/rsi_nifty_bank.png]]


*Average Directional Movemnet Index (ADX)* for NIFTY Bank Index...

#+begin_src python :session *py-session :results graphics file :file ../../img/adx_rsi_bank.png :exports both :cache yes
bank_hist['ADX']  = ta.ADX(bank_hist['High'], bank_hist['Low'],
                               bank_hist['Close'])
fig, axs = plt.subplots(3, 1, gridspec_kw={"height_ratios": [3, 1, 1]}, figsize=(10,8))
fig.suptitle("NIFTY Bank Index, RSI and ADX")
axs[0].plot(bank_hist['Close'])
axs[0].grid(axis='y')
axs[1].axhline(y=70, color='r', linestyle="--")
axs[1].axhline(y=30, color='g', linestyle="--")
axs[1].plot(bank_hist['RSI'], color='blue')
axs[1].set_ylabel("RSI")
axs[2].axhline(y=70, color='r', linestyle="--")
axs[2].axhline(y=30, color='g', linestyle="--")
axs[2].plot(bank_hist['ADX'], color='blue')
axs[2].set_ylabel("ADX")
fig
#+end_src

#+RESULTS[cb6f765d3bd84ee3060766a7007ee6b146340b25]:
[[file:../../img/adx_rsi_bank.png]]


* Comparing returns

Let's compare returns on Pvt. Banks, PSU Banks and NIFTY Bank index...

#+begin_src python :session *py-session :results graphics file :file ../../img/ret_banks.png :exports both :cache yes
norm_ret = pd.DataFrame(index = bank_hist.index)
norm_ret['pvt'] = (pvt_bank_hist['Close'] / pvt_bank_hist['Close'][0])
norm_ret['psu'] = (psu_bank_hist['Close'] / psu_bank_hist['Close'][0])
norm_ret['nifty'] = (bank_hist['Close'] / bank_hist['Close'][0])

fig, ax = plt.subplots(figsize = (8, 6))
fig.suptitle("Returns on Pvt, PSU and NIFTY Bank index.")
ax.plot(norm_ret['pvt'], label="PVT. Bank Index")
ax.plot(norm_ret['psu'], label="PSU Bank Index")
ax.plot(norm_ret['nifty'], label="NIFTY Bank Index")
ax.grid(axis='y')
ax.set_ylabel("% return on index")
ax.legend()
fig
#+end_src

#+RESULTS[dafd73e15d255cb69f0a7059437b68c243ba7601]:
[[file:../../img/ret_banks.png]]

* Percent Change

Private Bank Index % change:

#+begin_src python :session *py-session :results graphics file :file ../../img/pvt_bank_percent_change.png :exports both
pvt_bank_hist["pct_chg"]  = pvt_bank_hist['Close'].pct_change(periods=1).mul(100)
fig, ax = plt.subplots()
fig.suptitle("% Change in Pvt Bank Index")
ax.plot(pvt_bank_hist['pct_chg'])
ax.grid(axis='y')
fig
#+end_src

#+RESULTS:
[[file:../../img/pvt_bank_percent_change.png]]

Percent Change in PSU Bank Index ...

#+begin_src python :session *py-session :results graphics file :file ../../img/psu_bank_percent_change.png :exports both
psu_bank_hist["pct_chg"]  = psu_bank_hist['Close'].pct_change(periods=1).mul(100)
fig, ax = plt.subplots()
fig.suptitle("% Change in PSU Bank Index")
ax.plot(psu_bank_hist['pct_chg'])
ax.grid(axis='y')
fig
#+end_src

#+RESULTS:
[[file:../../img/psu_bank_percent_change.png]]

Percent change in NIFTY Bank Index ...

#+begin_src python :session *py-session :results graphics file :file ../../img/bank_percent_change.png :exports both
bank_hist["pct_chg"]  = bank_hist['Close'].pct_change(periods=1).mul(100)
fig, ax = plt.subplots()
fig.suptitle("% Change in NIFTY Bank Index")
ax.plot(bank_hist['pct_chg'])
ax.grid(axis='y')
fig
#+end_src

#+RESULTS:
[[file:../../img/bank_percent_change.png]]
