#!/usr/bin/env python3
import urllib
from breeze_connect import BreezeConnect
import datetime
from decouple import config
import pandas as pd
import mplfinance as mpf


# icici direct api
api_key = config("ISEC_AK_API_KEY")
secret_key = config("ISEC_AK_SEC_KEY")
session_key = config("SESSION_KEY")

print(
    "https://api.icicidirect.com/apiuser/login?api_key="
    + urllib.parse.quote_plus(api_key)
)

breeze = BreezeConnect(api_key=api_key)

# generate session
breeze.generate_session(api_secret=secret_key, session_token=session_key)


def iso_date(dt_str, t=True):
    """Convert date to ISO date string.
    Parameters:
    -----------
    dt_str: str, date in "%d/%m/%y %H:%M:%s" format,
    if t is False skip time component.
    t: bool, default True

    Return:
    -------
    str, date(time) in ISO format
    """
    if t:
        dt_fmt = "%d/%m/%Y %H:%M:%S"
        tz_str = ".000Z"
        res = datetime.datetime.strptime(dt_str, dt_fmt).isoformat()[:19] + tz_str
    else:
        dt_fmt = "%d/%m/%Y"
        tz_str = "T05:30:00.000Z"
        res = datetime.datetime.strptime(dt_str, dt_fmt).isoformat()[:10] + tz_str
    return res


# customer details
cust_detail = breeze.get_customer_details(api_session=session_key)
print(cust_detail["Status"])


def portfolio(exchange="NSE"):
    """Get Portfolio as Pandas dataframe.

    df columns: ticker, qty, avg_price, cmp, pl, percent_growth
    """
    portfolio_holdings = breeze.get_portfolio_holdings(exchange_code="NSE")

    # collect relevant data
    pf_data = {"ticker": [], "qty": [], "avg_price": [], "cmp": []}

    for _, tick in enumerate(portfolio_holdings["Success"]):
        pf_data["ticker"].append(tick["stock_code"])
        pf_data["qty"].append(tick["quantity"])
        pf_data["avg_price"].append(tick["average_price"])
        pf_data["cmp"].append(tick["current_market_price"])

    pf_df = pd.DataFrame.from_dict(pf_data)

    # change columns to numeric
    pf_df[["qty", "avg_price", "cmp"]] = pf_df[["qty", "avg_price", "cmp"]].apply(
        pd.to_numeric
    )

    pf_df["pl"] = pf_df["cmp"] - pf_df["avg_price"]
    pf_df["percent_growth"] = round((pf_df["pl"] / pf_df["avg_price"]) * 100, 2)
    return pf_df


# get historical data
bhaele = breeze.get_historical_data(
    interval="5minute",
    from_date="2024-02-02T09:00:00.000Z",
    to_date="2024-02-02T15:00:00.000Z",
    stock_code="BHAELE",
    exchange_code="NSE",
    product_type="cash",
)

hist_data = {"DateTime": [], "Open": [], "High": [], "Low": [], "Close": [], "Vol": []}
for _, item in enumerate(bhaele["Success"]):
    hist_data["DateTime"].append(item["datetime"])
    hist_data["Open"].append(item["open"])
    hist_data["High"].append(item["high"])
    hist_data["Low"].append(item["low"])
    hist_data["Close"].append(item["close"])
    hist_data["Vol"].append(item["volume"])

df_ticker = pd.DataFrame.from_dict(hist_data)

df_ticker[["Open", "High", "Low", "Close", "Vol"]] = df_ticker[
    ["Open", "High", "Low", "Close", "Vol"]
].apply(pd.to_numeric)

df_ticker["DateTime"] = pd.to_datetime(df_ticker["DateTime"])
df_ticker.drop([0], inplace=True)
df_ticker.set_index("DateTime", inplace=True)

df_ticker.head()
df_ticker.tail()

mpf.plot(df_ticker, style="yahoo")


def hist_data_plot(
    interval, from_date, to_date, ticker, exchange_code="NSE", product_type="cash"
):
    """Gives historical data of given equity and plot the OHLC data."""
    eq = breeze.get_historical_data(
        interval=interval,
        from_date=from_date,
        to_date=to_date,
        stock_code=ticker,
        exchange_code=exchange_code,
        product_type=product_type,
    )
    if eq["Status"] == 200:
        hist_data = {
            "DateTime": [],
            "Open": [],
            "High": [],
            "Low": [],
            "Close": [],
            "Vol": [],
        }
        for _, item in enumerate(eq["Success"]):
            hist_data["DateTime"].append(item["datetime"])
            hist_data["Open"].append(item["open"])
            hist_data["High"].append(item["high"])
            hist_data["Low"].append(item["low"])
            hist_data["Close"].append(item["close"])
            hist_data["Vol"].append(item["volume"])

        df_ticker = pd.DataFrame.from_dict(hist_data)

        df_ticker[["Open", "High", "Low", "Close", "Vol"]] = df_ticker[
            ["Open", "High", "Low", "Close", "Vol"]
        ].apply(pd.to_numeric)

        df_ticker["DateTime"] = pd.to_datetime(df_ticker["DateTime"])
        df_ticker.drop([0], inplace=True)
        df_ticker.set_index("DateTime", inplace=True)

        mpf.plot(df_ticker, type="line", style="yahoo")
        return df_ticker
    else:
        print("data not found...")


suzlon = hist_data_plot(
    interval="1day",
    from_date="2019-02-02T05:30:00.000Z",
    to_date="2024-02-02T16:30:00.000Z",
    ticker="SUZENE",
)

suzlon.info()
suzlon.to_csv("../data/suzlon.csv")

mpf.plot(suzlon, type="line", style="yahoo")

# Get fund details of account
breeze.get_funds()

# get names of stock
breeze.get_names(exchange_code="NSE", stock_code="SUZLON")

BIRNIF = hist_data_plot(
    interval="1day",
    from_date="2019-02-02T05:30:00.000Z",
    to_date="2024-02-02T16:30:00.000Z",
    ticker="BIRNIF",
)
